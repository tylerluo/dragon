Please see the section labeled "Quick Start Guide" within 
the AN1456.doc located the following directory.

\Micrium\AppNotes\AN1xxx-RTOS\AN1456\

This document will help you get the software compiled and
loaded in to the Dragon12-Plus EVB.  

Additional documentation may be found in the
\Micrium\Software\uCOS-II\DOC directory.
